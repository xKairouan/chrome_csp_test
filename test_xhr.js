// -*- coding: utf-8 -*-

(function () {
    'use strict';

    var next = document.querySelector('a[rel="next"]');
    if (!next) {
        return;
    }
    var url = next.href;
    var req = new XMLHttpRequest();
    req.addEventListener('load', function (e) {
        alert('In content script: ' + e.target.responseText.substr(0, 500));
    }, false);
    req.addEventListener('error', function (e) {
        alert('In content script: Error!');
    }, false);
    req.open('GET', url);
    req.send(null);

    chrome.extension.sendRequest({message: 'xhr', url: url}, function (response) {
        var message = 'In extension script: ';
        message += (response.status === 'load')?
            response.responseText.substr(0, 500): response.status;
        alert(message);
    });
}());
