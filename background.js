// -*- coding: utf-8 -*-

(function () {
    'use strict';

    chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
        switch (request.message) {
        case 'xhr':
            var req = new XMLHttpRequest();
            var response = {};
            try {
                req.addEventListener('load', function (e) {
                    response.status = 'load';
                    response.responseText = e.target.responseText;
                    sendResponse(response);
                }, false);
                req.addEventListener('error', function (e) {
                    response.status = 'error';
                    response.responseText = '';
                    sendResponse(response);
                }, false);
                req.open('GET', request.url);
                req.send(null);
            }
            catch (e) {
                sendResponse({status: 'error', responseText: ''});
            }
            break;
        default:
            break;
        }
    });
}());
